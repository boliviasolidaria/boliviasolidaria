const path = require('path');
const glob = require('glob');

const HTMLWebpackPlugin = require('html-webpack-plugin');

const generateHTMLPluginsProd = () => glob.sync('./src/views/*.html').map(
  dir => new HTMLWebpackPlugin({
    filename: path.basename(dir), // Output
    template: dir, // Input,
    lang: 'es',
    title: 'Bolivia Solidaria',
    minify: {
      collapseWhitespace: true,
      removeComments: true,
      removeRedundantAttributes: true,
      removeScriptTypeAttributes: true,
      removeStyleLinkTypeAttributes: true,
      useShortDoctype: true,
    },
  }),
);

const generateHTMLPluginsDev = () => glob.sync('./src/views/*.html').map(
  dir => new HTMLWebpackPlugin({
    filename: path.basename(dir), // Output
    template: dir, // Input
    title: 'Bolivia Solidaria',
    lang: 'es',
  }),
);

module.exports = {
  prod: generateHTMLPluginsProd,
  dev: generateHTMLPluginsDev,
} 
