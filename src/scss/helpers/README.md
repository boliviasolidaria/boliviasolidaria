# Helpers - Abstracts - Utils

La carpeta `helpers/` reúne todas las herramientas y helpers de Sass utilizadas en todo el proyecto. Todas las variables, funciones, mixins y placeholders deben colocarse aquí.

La regla general para esta carpeta es que no debe generar una sola línea de CSS cuando se compila por sí sola. Estos no son más que helpers de Sass.

Referencia: [Sass Guidelines](http://sass-guidelin.es/) > [Architecture](http://sass-guidelin.es/#architecture) > [Abstracts folder](http://sass-guidelin.es/#abstracts-folder)