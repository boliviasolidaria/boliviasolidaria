# Vendors

La mayoría de los proyectos tendrán una carpeta `vendors/` que contendrá todos los archivos CSS de librerías y frameworks externos: Normalize, Bootstrap, jQueryUI, FancyCarouselSliderjQueryPowered, etc. Ponerlos en la misma carpeta es una buena manera de decir "Esto no es mío, no es mi código, no es mi responsabilidad".

Si tiene que sobreescribir una sección de cualquier librería externa, le recomiendo que tenga una octava carpeta llamada `vendors-extensions /` en la que puede tener archivos nombrados exactamente como las librerías que se sobrescriben. Por ejemplo, `vendors-extensions / _bootstrap.scss` es un archivo que contiene todas las reglas CSS destinadas a volver a declarar algunos de los CSS predeterminados de Bootstrap. Esto es para evitar editar los archivos del autor, lo que generalmente no es una buena idea.

Reference: [Sass Guidelines](http://sass-guidelin.es/) > [Architecture](http://sass-guidelin.es/#architecture) > [Vendors folder](http://sass-guidelin.es/#vendors-folder)