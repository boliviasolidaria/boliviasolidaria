# Components

Para pequeños componentes, existe la carpeta `components/`. Mientras que `layout/` es macro ( que define la estructura global ), `components/` está más enfocado a widgets. Contiene todo tipo de módulos específicos como un `slider`, un `loader`, un `widget` y básicamente lo que sea en ese lineamiento. Por lo general, hay muchos archivos en la carpeta `components/` ya que todo el sitio/aplicación debe estar compuesto principalmente por pequeños módulos.


Referencia: [Sass Guidelines](http://sass-guidelin.es/) > [Architecture](http://sass-guidelin.es/#architecture) > [Components folder](http://sass-guidelin.es/#components-folder)