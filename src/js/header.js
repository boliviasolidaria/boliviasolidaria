function onLoadHeader() {
    const buttonsMenu = [
        document.getElementById('btn-menu-mobile-1'),
        document.getElementById('btn-menu-mobile-2'),
        document.getElementById('btn-menu-mobile-3')
    ];

    buttonsMenu.forEach(button => {
        button.addEventListener('click', handleClickBtn);
    });

    function handleClickBtn(e) {
        const targetId = this.getAttribute('data-target');
        const target = document.querySelector(targetId);
        
        closeButtonsExceptCurrent(buttonsMenu, targetId);

        this.classList.toggle('active');
        target.classList.toggle('show');
        document.body.classList.add('lock-scroll');
    }

    function closeButtonsExceptCurrent(buttons, id) {
        buttons.forEach((button) => {
            const targetId = button.getAttribute('data-target');
            const target = document.querySelector(targetId);

            if (targetId !== id) {
                button.classList.remove('active');
                target.classList.remove('show');
            }
        });
    }
}

document.addEventListener('DOMContentLoaded', onLoadHeader);